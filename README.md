# This is my package filament-livewire-modal

[![Latest Version on Packagist](https://img.shields.io/packagist/v/kda/filament-livewire-modal.svg?style=flat-square)](https://packagist.org/packages/kda/filament-livewire-modal)
[![Total Downloads](https://img.shields.io/packagist/dt/kda/filament-livewire-modal.svg?style=flat-square)](https://packagist.org/packages/kda/filament-livewire-modal)

## Installation

You can install the package via composer:

```bash
composer require kda/filament-livewire-modal
```

You can publish and run the migrations with:

```bash
php artisan vendor:publish --provider="\KDA\Filament\LivewireModal\ServiceProvider" --tag="migrations"
php artisan migrate
```

You can publish the config file with:

```bash
php artisan vendor:publish --provider="\KDA\Filament\LivewireModal\ServiceProvider" --tag="config"
```

This is the contents of the published config file:

```php
return [
];
```

Optionally, you can publish the views using

```bash
php artisan vendor:publish --provider="\KDA\Filament\LivewireModal\ServiceProvider" --tag="views"
```

## Usage

```php
$filament\LivewireModal = new KDA\Filament\LivewireModal();
echo $filament\LivewireModal->echoPhrase('Hello, KDA!');
```

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](SECURITY.md) on how to report security vulnerabilities.

## Credits

- [Fabien Karsegard](https://github.com/fdt2k)
- [All Contributors](CONTRIBUTORS.md)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
Please see [License File](LICENSE.KDA.md) for more information.
