<?php

namespace KDA\Filament\LivewireModal;

use Filament\PluginServiceProvider;
use Spatie\LaravelPackageTools\Package;
use Livewire\Livewire;
use Filament\Facades\Filament;
use Illuminate\Support\Facades\Blade;

class FilamentServiceProvider extends PluginServiceProvider
{
    protected array $styles = [
        //    'my-package-styles' => __DIR__ . '/../dist/app.css',
    ];

    protected array $widgets = [
        //    CustomWidget::class,
    ];

    protected array $pages = [
        //    CustomPage::class,
    ];

    protected array $resources = [
        //     CustomResource::class,
    ];

    protected array $relationManagers = [];

    public function configurePackage(Package $package): void
    {
        $package->name('filament-livewire-modal');
    }

    public function packageBooted(): void
    {
        parent::packageBooted();
        Filament::registerRenderHook(
            'body.start',
            fn (): string => Blade::render('@livewire(\'livewire-ui-modal\')'),
        );
    }
}
